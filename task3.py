"""TASK 3"""
from flask import Flask, jsonify, request

app = Flask(__name__)


def decorator(func):
    """Decorator for inverse the operator"""
    def wrapper(op1, op2, operator):
        """wrapper function"""
        if operator == "+":
            operator = "-"
        elif operator == "-":
            operator = "+"
        return func(op1, op2, operator)
    return wrapper


@decorator
def cal(op1, op2, operator):
    """perform operation on basis operator"""
    try:
        if operator == "+":
            return op1 + op2
        if operator == "-":
            return op1 - op2
        return "Invalid"
    except Exception as e:
        print(e)


@app.route("/", methods=["POST"])
def operate():
    """Route this function on /"""
    req_data = request.get_json()
    result = cal(req_data["op1"], req_data["op2"], req_data["op"])

    return jsonify(
        {
            "result": result
        })


if __name__ == "__main__":
    app.run(debug=True)
