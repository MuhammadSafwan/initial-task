"""TASK 5"""
from flask import Flask, request, jsonify
from bson.json_util import dumps
import pymongo

app = Flask(__name__)

my_client = pymongo.MongoClient("mongodb://localhost:27017/")
my_db = my_client["Task5"]
CAL_COLL = my_db["calculations"]
LAST_OP_COLL = my_db["last_operation"]


def calculate(op1, op2, operator):
    """calculate result on basis of operator"""
    try:
        if operator == "+":
            return op1 + op2
        if operator == "-":
            return op1 - op2
        if operator == "*":
            return op1 * op2
        if operator == "/":
            return op1 / op2
        return "invalid Operator"
    except Exception as e:
        print(e)


def insert_in_calculation(doc):
    """Update the calculation document in mongodb"""
    if doc["op"] in ["+", "-", "*", "/"] and doc["result"] is not None:
        cursor = CAL_COLL.find({}, {"_id": 1}).sort("_id", -1).limit(1)
        maximum_id = list(cursor)
        if len(maximum_id) == 0:
            my_id = 1
        else:
            maximum_id = maximum_id[0]["_id"]
            my_id = maximum_id + 1
        doc["_id"] = my_id
        CAL_COLL.insert_one(doc)


def update_last_operation(doc):
    """Update the last_operation document in mongodb"""
    if doc["op"] in ["+", "-", "*", "/"] and doc["result"] is not None:
        query = {"op": doc["op"]}
        new_value = {"$set": {
            "op1": doc["op1"],
            "op2": doc["op2"],
            "result": doc["result"]
        }}
        print(doc)
        # my_query = { "address": "Valley 345" }
        # new_values = { "$set": { "address": "Canyon 123" } }
        # LAST_OP_COLL.save(query, doc, upsert=True)
        LAST_OP_COLL.update_one(query, new_value, upsert=True)


@app.route("/", methods=["POST"])
def operation():
    """Route to this function on /"""
    req_data = request.get_json()
    result = calculate(req_data["op1"], req_data["op2"], req_data["op"])
    doc = req_data
    doc["result"] = result
    insert_in_calculation(doc)
    update_last_operation(doc)
    # return doc
    return jsonify(
        {
            "result": result
        }
    )


@app.route("/calculations", methods=["GET"])
def show_calculations():
    """Route to this function on /calculations"""
    query_result = CAL_COLL.find()
    leng = list(query_result)
    total_records = len(leng)
    temp = {"Total Records": total_records}
    leng.append(temp)
    calculations = dumps(leng)
    return calculations


@app.route("/latest operation", methods=["GET"])
def show_last_operation():
    """Route to this function on /show_last_operation"""
    query_result = LAST_OP_COLL.find()
    leng = list(query_result)
    total_records = len(leng)
    temp = {"Total Records": total_records}
    leng.append(temp)
    calculations = dumps(leng)
    return calculations


if __name__ == "__main__":
    app.run(debug=True)
