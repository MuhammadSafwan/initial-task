"""Task 2"""


from flask import Flask, jsonify, request
app = Flask(__name__)
# Comment


def cal(op1, op2, operator):
    """perform operation on basis of operator"""
    try:
        if operator == "+":
            return op1+op2
        return "Invalid"
    except Exception as e:
        print(e)


@app.route("/", methods=["POST"])
def operate():
    """operate this function on /"""
    req_data = request.get_json()
    result = cal(req_data["op1"], req_data["op2"], req_data["op"])
    print("RESULT IS", result)
    return jsonify(
        {
            "result": result
        })


if __name__ == "__main__":
    app.run(debug=True)
